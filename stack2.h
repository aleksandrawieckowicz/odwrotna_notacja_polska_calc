#include <stdio.h>
#include <stdlib.h>

// Data structure to represent a stack
struct stack2
{
    int maxsize;    // define max capacity of the stack
    int top;
    float *items;
};

// Utility function to initialize the stack
struct stack2* newStack2(int capacity)
{
    struct stack2 *pt = (struct stack2*)malloc(sizeof(struct stack2));

    pt->maxsize = capacity;
    pt->top = -1;
    pt->items = (float*)malloc(sizeof(float) * capacity);

    return pt;
}

// Utility function to return the size of the stack
int size2(struct stack2 *pt) {
    return pt->top + 1;
}

// Utility function to check if the stack is empty or not
int isEmpty2(struct stack2 *pt) {
    return pt->top == -1;                   // or return size(pt) == 0;
}

// Utility function to check if the stack is full or not
int isFull2(struct stack2 *pt) {
    return pt->top == pt->maxsize - 1;      // or return size(pt) == pt->maxsize;
}

// Utility function to add an element `x` to the stack
void push2(struct stack2 *pt, float x)
{
    // check if the stack is already full. Then inserting an element would
    // lead to stack overflow
    if (isFull2(pt))
    {
        //printf("Overflow\nProgram Terminated\n");
        exit(EXIT_FAILURE);
    }

    //printf("Inserting %f\n", x);

    // add an element and increment the top's index
    pt->items[++pt->top] = x;
}

// Utility function to return the top element of the stack
float peek2(struct stack2 *pt)
{
    // check for an empty stack
    if (!isEmpty2(pt)) {
        return pt->items[pt->top];
    }
    else {
        exit(EXIT_FAILURE);
    }
}

// Utility function to pop a top element from the stack
float pop2(struct stack2 *pt)
{
    // check for stack underflow
    if (isEmpty2(pt))
    {
       // printf("Underflow\nProgram Terminated\n");
        exit(EXIT_FAILURE);
    }

    //printf("Removing %d\n", peek2(pt));

    // decrement stack size by 1 and (optionally) return the popped element
    return pt->items[pt->top--];
}


