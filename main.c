#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <math.h>
#include "stack.h" //biblioteka obslugujaca stos
#include "stack2.h"


    char instr[] = "Podaj wyrazenie korzystajac z podanych symboli: \n"  //tekst instrukcji obslugi
                    "\t( ) - nawiasy,\n"
                    "\t. - separator dziesietny\n"
                    "oraz operatorow matematycznych:\n"
                    "\t+ - dodawanie,\n"
                    "\t- - odejmowanie,\n"
                    "\t* - mnozenie,\n"
                    "\t/ - dzielenie,\n"
                    "\t^ - potegowanie.\n"
                    "Na koncu kazdego wyrazenia umiesc znak =\n"
                    "oraz potwierdz klawiszem ENTER.\n"
                    "\tPODAJ WYRAZENIE DO OBLICZENIA:";

    char prior0[11] = {0,1,2,3,4,5,6,7,8,9,'.'}; // pogrupowane znaki, ktore obsluguje program, kazda grupa ma charakt. priorytet
    char prior1[1] = {'('};
    char prior2[2] = {'+','-'};
    char prior3[2] = {'*','/'};
    char prior4[1] = {'^'};
    char closingBrace5[1] = {')'};
    char equal6[1] = {'='};

    struct numbOrOper {                                     //struktura przechowujaca pola w zaleznosci od typu, liczba(jesli liczba to isItNumber=1 lub znak
        double numb;
        char mathOper;
        int isItNumber;
    };

    int findSourceTab(char sign, char tab[],int tabLength); //ponizej funkcje zaimplementowane pod main()
    int getPriority (char sign);
    double singleChartoDoub(char ch);
    void handleNegNumb(int j,struct numbOrOper tab[],struct stack *st);
    void forbidDivByZero(char tab[], int tabLength);



int main()
{
//cz. 1 - konwertowanie z notacji inifiksowej do ONP:
    char insertedExpr[20];

    puts(instr);                                               //wyswietlenie instrukcji
    scanf("%s",&insertedExpr);                                 //wczytywanie wpisanego wyrazenia do tablicy znakow
    forbidDivByZero(insertedExpr, findTabLength(insertedExpr));//sprawdzenie czy w wyrazeniu nie ma dzielenia przez 0

    struct numbOrOper exitTab[20];
    struct stack *st = newStack(20);
    int exprLength = findTabLength(insertedExpr);
    int j = 0;
    int isPrevPrior0;
    int isFirstMinus;
    char wholeNumb[20];
    int k;
    char sign;

    for (int i = 0; i<exprLength; i++){                        //petla analizujaca kazdy znak tablicy zawierajacej wyrazenie
       sign = insertedExpr[i];

       switch(getPriority(sign))                               //instrukcja wykonujaca poszczegolne dzialanie w zal. od priorytetu danego znaku
       {
        case 5:
            while(!isEmpty(st) && peek(st) != '('){            //petla wykonujaca sie poki stos nie jest pusty i wierzcholek stosu nie jest rowny '('
                exitTab[j].mathOper = pop(st);                 //przypisywanie kolejnym polom o ident. matchOper znakow ze stosu
                j++;                                           // zwieksz indeks tablicy wyjscie o 1
            }
            pop(st);                                           //wyrzuc '(' ze stosu
            isPrevPrior0 = 0;                                  //przypisanie zmiennej isPrevPrior wartosci 0 informuje, ze poprzedni znak nie byl z grupy "prior0"
            break;
        case 4:
             while(!isEmpty(st) && getPriority(peek(st))==4){  //petla wykonujaca sie poki stos nie jest pusty i priorytet wierzcholka stosu jest rowny 4
                exitTab[j].mathOper = pop(st);
                j++;
             }
             push(st, sign);                                   //dodaj rozpatrywany znak do stosu
             isPrevPrior0 = 0;
             break;
        case 3:
            while(!isEmpty(st) && getPriority(peek(st))>=3){   //petla wykonujaca sie poki stos nie jest pusty i priorytet wierzcholka stosu jest wiekszy lub rowny 3
                exitTab[j].mathOper = pop(st);
                j++;
            }
             push(st, sign);
             isPrevPrior0 = 0;
             break;
        case 2:
            if(i==0 && sign == '-'){                           //warunek sprawdzajacy czy indeks tablicy z wyrazeniem jest rowny zero oraz znak to '-'
                isFirstMinus = 1;                              //przypisz zmiennej wartosc true jesli pierwszym znakiem jest minus
            }else if(insertedExpr[i-1] == '(' && sign == '-'){ //warunek sprawdzajacy czy rozpatrywana jest liczba ujemna "(-..."
                handleNegNumb(j, exitTab, st);                 //funkcja obsługująca liczby ujemne
                j++;
            }else{
                while(!isEmpty(st) && getPriority(peek(st))>=2){//petla wykonujaca sie poki stos nie jest pusty i priorytet wierzcholka stosu jest wiekszy lub rowny 2
                    exitTab[j].mathOper = pop(st);
                    j++;
                }
            push(st, sign);
            }
            isPrevPrior0 = 0;
            break;
        case 1:
            if(isFirstMinus == 1){
                handleNegNumb(j, exitTab, st);
                j++;
                isFirstMinus = 0;
            }
            push(st, sign);
            isPrevPrior0 = 0;
            break;
        case 0:
            if(isPrevPrior0 == 1){                             //warunek sprawdzajacy czy poprzedni znak mial priorytet rowny 1, jesli tak tzn,ze liczba ma wiecej niz 1 cyfre
                wholeNumb[k] = sign;                           //przypisz kolejn. elem. tablicy znakow kolejna cyfre liczby
                exitTab[j].numb = strtod(wholeNumb, NULL);     //przypisz polu .numb cala liczbe przekonwertowana na typ double

            } else {
                k = 0;
                if(isFirstMinus == 1){                         //jesli zmienna jest rowna 1 tzn, ze na poczatku calego wyrazenia stoi '-' a po nim cyfra
                    handleNegNumb(j, exitTab, st);
                    j++;
                    wholeNumb[k] = sign;                       //przypisz rozpatrywany znak jako pierwsza cyfre liczby
                    exitTab[j].numb = singleChartoDoub(sign);
                    isFirstMinus = 0;
                } else {                                       //sytuacja gdy rozpatrywany znak bedzie pierwsza cyfra a '-' nie stoi na poczatku calego wyrazenia
                    wholeNumb[k] = sign;                       //przypisz rozpatrywany znak jako pierwsza cyfre liczby
                    exitTab[j].numb = singleChartoDoub(sign);
                }
            }
            exitTab[j].isItNumber = 1;                         //przypisz polu .isItNumber wartosc 1, to pole pomaga przy wyswietlaniu za pomoca struktury koncowego wyrazenia w ONP
            k++;                                               //zwieksz indeks tablicy cyfr gdyby nastepny znak byl cyfra

            if(getPriority(insertedExpr[i+1])!= 0){            //warunek sprawdzajacy czy nastepny znak jest cyfra
               memset(wholeNumb, 0, sizeof wholeNumb);         //czyszczenie tablicy znakow
               j++;
            }
            isPrevPrior0 = 1;                                  //zasygnalizuj, ze poprzedni znak byl cyfra
            break;
        case 6:
            while(!isEmpty(st)){                               //petla wykonujaca sie poki stos nie jest pusty
                exitTab[j].mathOper = pop(st);                 //wyrzucanie wszystkich elementow ze stosu
                j++;
            }
            break;
        }
    }

    printf("Wyrazenie w ONP: ");                               //komunikat poprzedzajacy wyswietlenie wyrazenia ONP w konsoli
    for (int i=0; i<j; i++){                                   //petla obslugujaca poszczegolne elementy tablicy
        if(exitTab[i].isItNumber ==1 )                         //warunek sprawdzajacy czy dany element tablicy struktur jest liczba
        printf(" %f ", exitTab[i].numb);                       //wyswietlenie elementu

        if(exitTab[i].isItNumber != 1)                         //warunek sprawdzajacy czy dany element tablicy struktur nie jest liczba
        printf(" %c ", exitTab[i].mathOper);                   //wyswietlenie elementu
    }
    printf("\n");

//cz. 2 - obliczanie wartosci ONP:
    struct stack2 *st2 = newStack2(20);           //tworzenie stosu przechowujacego dane typu float
    double result;                                //zmienna przechowujaca tymczasowy wynik
    double x;                                     //zmienna symbolizujaca jedna liczbe wyrazenia
    double y;                                     //zmienna symbolizujaca druga liczbe wyrazenia
    for(int i = 0; i<j; i++){                     //petla odczytujaca kolejne znaki z wyrazenia ONP, ktore przechowuje exitTab
        if(exitTab[i].isItNumber == 1){           //jesli pierwszy znak z wyrazenia jest liczba...
            push2(st2, exitTab[i].numb);          //odloz go na stos
        }else{
            result = 0;                          //w innym przypadku przypisz wynikowi zero
            switch(exitTab[i].mathOper){         //gdy znak jest operatorem matematycznym...
                case '+':                        //...plusem
                     x = pop2(st2);              //wyjmij z wierzcholka stosu liczbe...
                     y = pop2(st2);              //...oraz kolejną...
                     result = y + x;             //...i oblicz wynik
                     push2(st2, result);         //dodaj ten wynik do stosu
                     break;                      //przerwij ten obieg petli
                case '-':                        //gdy znak jest minusem...
                     x = pop2(st2);
                     y = pop2(st2);              //   --||--
                     result = y - x;
                     push2(st2, result);
                     break;
                case '*':                        //   --||--
                    result = pop2(st2)*pop2(st2);
                    push2(st2, result);
                    break;
                case '/':                        //   --||--
                    x = pop2(st2);
                    y = pop2(st2);
                    result = y/x;
                    push2(st2, result);
                    break;
                case '^':                       //   --||--
                    x = pop2(st2);
                    y = pop2(st2);
                    result = pow(y,x);
                    push2(st2, result);
                    break;
            }
        }
    }
    double finalResult = pop2(st2);            //ostatni znak na stosie po zakonczeniu wszystkich petli to wynik(wy
    printf("Wynik : %f\n\n", finalResult);     //wyswietl ostateczny wynik

   return main();                              //uruchom program od nowa
}


int findSourceTab(char sign, char tab[], int tabLength){       //funkcja weryfikujaca czy podany znak w argumencie znajduje sie w podanej tablicy znakow

    for(int i=0; i<tabLength; i++){
        if(sign == tab[i]){
            return 1;
        }
    }
    return 0;
}

int getPriority (char sign){                                   //funkcja zwracajaca priorytet podanego w argumencie znaku

     if(findSourceTab(sign, closingBrace5, 1)==1)
        return 5;
     if(findSourceTab(sign, prior4, 1)==1)
        return 4;
     if(findSourceTab(sign, prior3, 2)==1)
        return 3;
     if(findSourceTab(sign, prior2, 2)==1)
        return 2;
     if(findSourceTab(sign, prior1, 1)==1)
        return 1;
     if(findSourceTab(sign, prior0, 11)==1)
        return 0;
     if(findSourceTab(sign, equal6, 1)==1)
        return 6;
}

int findTabLength(char expr[]){                                       //funkcja obliczajaca dlugosc tablicy
     int i = 0;
     do{
        i++;
     }while (expr[i] != '=');
     return i+1;
}

double singleChartoDoub(char ch){                                    //funkcja konwertujaca pojedynczy znak typu char na typ double
    char str[2];
    str[0] = ch;
    str[1] = '\0';
return strtod(str, NULL);
}

void handleNegNumb(int j,struct numbOrOper tab[], struct stack *st){ //funkcja obslugujaca ujemne liczby poprzez pomnożenie danej liczby przez '-1'
        tab[j].numb = -1;
        tab[j].isItNumber = 1;
        push(st, '*');
}

void forbidDivByZero(char tab[], int tabLength){                     //funkcja konczaca program gdy napotka w wyrazeniu dzielenie przez 0

    for(int i = 0; i<tabLength; i++){
        if(tab[i] == '/' && tab[i+1] == '0'){
            puts("NIE DZIEL PRZEZ 0!!!");
            exit(1);
        }
    }
}





